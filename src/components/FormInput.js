import React, { useState } from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { Radio, Form, Button, Typography } from 'antd';
import {
  changeRegion,
  toggleRegionInfo,
  changeMarkerCoords,
} from '../redux/actions';
import countries from '../utils/countries_info.json';
import '../utils/css/formInput.css';

const { Title } = Typography;

function FormInput({
  region,
  darkModeStatus,
  changeRegion: onChangeRegion,
  toggleRegionInfo: onToggleRegionInfo,
  changeMarkerCoords: onChangeMarkerCoords,
}) {
  const [form] = Form.useForm();
  const [currentRegion, setRegion] = useState(region);

  const onChange = (e) => {
    setRegion(e.target.value);
  };

  const onLoad = () => {
    onChangeRegion(currentRegion);
    onToggleRegionInfo(true);
    onChangeMarkerCoords(countries[currentRegion].coords);
  };

  return (
    <Form
      form={form}
      layout="vertical"
      className={`form-input ${darkModeStatus && 'dark-mode'}`}
    >
      <Title level={3} className={darkModeStatus && 'dark-mode'}>
        Select a country
      </Title>
      <Radio.Group
        onChange={onChange}
        value={currentRegion}
        className={darkModeStatus && 'dark-mode'}
      >
        <Radio
          value="india"
          className={`form-radio-btn ${darkModeStatus && 'dark-mode'}`}
        >
          India
        </Radio>
        <br />
        <Radio
          value="united_states"
          className={`form-radio-btn ${darkModeStatus && 'dark-mode'}`}
        >
          United States
        </Radio>
        <br />
        <Radio
          value="united_kingdom"
          className={`form-radio-btn ${darkModeStatus && 'dark-mode'}`}
        >
          United Kingdom
        </Radio>
      </Radio.Group>
      <Form.Item>
        <Button type="primary" onClick={onLoad}>
          Load
        </Button>
      </Form.Item>
    </Form>
  );
}

const mapStateToProps = (state) => {
  return {
    region: state.regionSelector.region,
    darkModeStatus: state.sidebarActivity.darkModeStatus,
  };
};

FormInput.propTypes = {
  region: PropTypes.string.isRequired,
  darkModeStatus: PropTypes.bool.isRequired,
  toggleRegionInfo: PropTypes.func.isRequired,
  changeRegion: PropTypes.func.isRequired,
  changeMarkerCoords: PropTypes.func.isRequired,
};

export default connect(mapStateToProps, {
  changeRegion,
  toggleRegionInfo,
  changeMarkerCoords,
})(FormInput);
