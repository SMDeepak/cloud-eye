import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import {
  MapContainer,
  TileLayer,
  Marker,
  useMapEvents,
  useMap,
  GeoJSON,
  // Polygon,
} from 'react-leaflet';
import Leaflet from 'leaflet';
import * as turf from '@turf/turf';
import { changeMarkerCoords, toggleRegionInfo } from '../redux/actions';
import gpsIcon from '../utils/images/gps-icon.svg';

function Map({
  mainMarker,
  showRegionInfo,
  circleRadius,
  showCircle,
  changeMarkerCoords: onChangeMarkerCoords,
  toggleRegionInfo: onToggleRegionInfo,
}) {
  let parentMap;
  const Markers = () => {
    // We need to change the map view (by using '.setView()') to the location that the user had selected to load in the form input.
    // Making sure that this doesn't occur while the user selects a location instead of loading it.
    // So, this lends a difference between loading and selecting a location.
    // That difference can be made by hiding the region info form while the user select a location and change the map view only if the region info form is visible.
    useMapEvents({
      click(e) {
        // hide the region info form if user selects a location
        onToggleRegionInfo(false);
        onChangeMarkerCoords([e.latlng.lat, e.latlng.lng]);
      },
    });

    // check whether the region info form is visible [(i.e) it's a loaded location]
    if (showRegionInfo) {
      parentMap = useMap();
      parentMap.setView(mainMarker);
    }
    return mainMarker ? (
      <Marker
        key="marker"
        position={mainMarker}
        interactive={false}
        icon={
          new Leaflet.Icon({
            iconUrl: gpsIcon,
            iconRetinaUrl: gpsIcon,
            iconSize: [38, 95],
            iconAnchor: [22, 66],
            popupAnchor: [-3, -76],
          })
        }
      />
    ) : null;
  };
  const Draw = () => {
    if (showCircle) {
      const radius = circleRadius;
      const options = {
        steps: 100,
        units: 'kilometers',
        properties: { foo: 'bar' },
      };
      const marker = [...mainMarker];
      const circle = turf.circle(marker.reverse(), radius, options);
      return (
        <GeoJSON
          data={Leaflet.geoJSON(circle, {
            coordsToLatLng: (coords) =>
              // reverse the lat long coordinates to match the GeoJSON component's behaviour
              new Leaflet.LatLng(coords[1], coords[0], coords[2]),
          }).toGeoJSON()}
        />
      );
    }
    return null;
  };

  return (
    <MapContainer center={mainMarker} zoom={5} scrollWheelZoom>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Markers />
      <Draw />
    </MapContainer>
  );
}

Map.propTypes = {
  mainMarker: PropTypes.arrayOf(PropTypes.string).isRequired,
  showCircle: PropTypes.bool.isRequired,
  circleRadius: PropTypes.number.isRequired,
  showRegionInfo: PropTypes.bool.isRequired,
  changeMarkerCoords: PropTypes.func.isRequired,
  toggleRegionInfo: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    mainMarker: state.mapData.mainMarker,
    circleRadius: state.sidebarActivity.circleRadius,
    showCircle: state.sidebarActivity.showCircle,
    isLoadedCoords: state.mapData.isLoadedCoords,
    showRegionInfo: state.regionInfo.showRegionInfo,
  };
};

export default connect(mapStateToProps, {
  changeMarkerCoords,
  toggleRegionInfo,
})(Map);
