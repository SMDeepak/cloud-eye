import React from 'react';
import { PropTypes } from 'prop-types';
import { Divider, Typography } from 'antd';
import { connect } from 'react-redux';
import { toggleRegionInfo } from '../redux/actions';
import countries from '../utils/countries_info.json';
import '../utils/css/regionInfo.css';

const { Title } = Typography;
function RegionInfo({ region, showRegionInfo, darkModeStatus }) {
  const regionDetails = countries[region];
  if (!showRegionInfo) return null;
  return (
    <div className={`region-info ${darkModeStatus && 'dark-mode'}`}>
      <div
        style={{
          backgroundImage: `url(${regionDetails.imgUrl})`,
        }}
        className="region-img"
      />
      <div style={{ marginTop: '24px', marginBottom: '24px' }}>
        <Title
          level={3}
          style={{ textAlign: 'center' }}
          className={darkModeStatus && 'dark-mode'}
        >
          {regionDetails.title}
        </Title>
        <div className="line-separator" />
        <Divider orientation="left" className={darkModeStatus && 'dark-mode'}>
          Currency
        </Divider>
        <p className="region-info-item">{regionDetails.currency}</p>
        <Divider orientation="left" className={darkModeStatus && 'dark-mode'}>
          Speed
        </Divider>
        <p className="region-info-item">{regionDetails.speed}</p>
        <Divider orientation="left" className={darkModeStatus && 'dark-mode'}>
          Distance
        </Divider>
        <p className="region-info-item">{regionDetails.distance}</p>
        <Divider orientation="left" className={darkModeStatus && 'dark-mode'}>
          Volume
        </Divider>
        <p className="region-info-item">{regionDetails.volume}</p>
        <Divider orientation="left" className={darkModeStatus && 'dark-mode'}>
          Timezones
        </Divider>
        <p className="region-info-item">{regionDetails.timezones}</p>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    region: state.regionSelector.region,
    showRegionInfo: state.regionInfo.showRegionInfo,
    darkModeStatus: state.sidebarActivity.darkModeStatus,
  };
};

RegionInfo.propTypes = {
  region: PropTypes.string.isRequired,
  showRegionInfo: PropTypes.bool.isRequired,
  darkModeStatus: PropTypes.bool.isRequired,
};

export default connect(mapStateToProps, { toggleRegionInfo })(RegionInfo);
