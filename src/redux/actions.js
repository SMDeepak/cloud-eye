export const changeDistance = (distance) => ({
  type: 'CHANGE_DISTANCE',
  payload: {
    distance,
  },
});

export const addMarker = (coords) => ({
  type: 'ADD_MARKER',
  payload: {
    markers: [coords],
  },
});

export const changeRegion = (region) => ({
  type: 'CHANGE_REGION',
  payload: {
    region,
  },
});

export const toggleRegionInfo = (showStatus) => ({
  type: 'TOGGLE_REGION_INFO',
  payload: {
    showStatus,
  },
});

export const changeMarkerCoords = (coords) => ({
  type: 'CHANGE_MARKER_COORDS',
  payload: {
    mainMarker: coords,
  },
});

export const changeFloaterVisibility = (status) => ({
  type: 'CHANGE_FLOATER_VISIBILITY',
  payload: {
    floatersVisibility: status,
  },
});

export const changeCircleStatus = (showCircle) => ({
  type: 'CHANGE_CIRCLE_STATUS',
  payload: {
    showCircle,
  },
});

export const changeCircleRadius = (circleRadius) => ({
  type: 'CHANGE_CIRCLE_RADIUS',
  payload: {
    circleRadius,
  },
});

export const changeDarkMode = (darkModeStatus) => ({
  type: 'CHANGE_DARK_MODE',
  payload: {
    darkModeStatus,
  },
});
