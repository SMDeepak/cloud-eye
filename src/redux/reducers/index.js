import { combineReducers } from 'redux';
import regionSelector from './regionSelector';
import regionInfo from './regionInfo';
import mapData from './mapData';
import sidebarActivity from './sidebarActivity';

export default combineReducers({
  regionInfo,
  regionSelector,
  mapData,
  sidebarActivity,
});
