const initialState = {
  region: 'india',
};

export default function regionSelector(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_REGION': {
      const { region } = action.payload;
      return { ...state, region };
    }
    default:
      return state;
  }
}
