const initialState = {
  showRegionInfo: false,
};

export default function regionInfo(state = initialState, action) {
  switch (action.type) {
    case 'TOGGLE_REGION_INFO': {
      const { showStatus } = action.payload;
      return { ...state, showRegionInfo: showStatus };
    }
    default: {
      return state;
    }
  }
}
