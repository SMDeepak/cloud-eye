const initialState = {
  mainMarker: [20.5937, 78.9629],
};

export default function mapData(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_MARKER_COORDS': {
      return { mainMarker: action.payload.mainMarker };
    }
    default:
      return state;
  }
}
