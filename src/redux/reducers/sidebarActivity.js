const initialState = {
  floatersVisibility: true,
  showCircle: false,
  circleRadius: 500,
  darkModeStatus: false,
};

export default function sidebarActivity(state = initialState, action) {
  switch (action.type) {
    case 'CHANGE_FLOATER_VISIBILITY': {
      return {
        ...state,
        floatersVisibility: action.payload.floatersVisibility,
      };
    }
    case 'CHANGE_CIRCLE_STATUS': {
      return {
        ...state,
        showCircle: action.payload.showCircle,
      };
    }
    case 'CHANGE_CIRCLE_RADIUS': {
      return {
        ...state,
        circleRadius: action.payload.circleRadius,
      };
    }
    case 'CHANGE_DARK_MODE': {
      return {
        ...state,
        darkModeStatus: action.payload.darkModeStatus,
      };
    }
    default:
      return state;
  }
}
