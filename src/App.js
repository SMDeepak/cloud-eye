import React, { useState } from 'react';
import {
  Layout,
  Menu,
  Button,
  Dropdown,
  Checkbox,
  Switch,
  InputNumber,
} from 'antd';
import {
  UserOutlined,
  CloseCircleOutlined,
  AppstoreOutlined,
  DownOutlined,
  CiCircleOutlined,
} from '@ant-design/icons';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import {
  changeFloaterVisibility,
  changeCircleRadius,
  changeCircleStatus,
  changeDarkMode,
} from './redux/actions';
import FormInput from './components/FormInput';
import 'leaflet/dist/leaflet.css';
import 'antd/dist/antd.css';
import logo from './utils/images/cloud-eye-logo.png';
import Map from './components/Map';
import RegionInfo from './components/RegionInfo';
import './App.css';

const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;

function App({
  floatersVisibility,
  circleRadius,
  showCircle,
  darkModeStatus,
  changeCircleStatus: onChangeCircleStatus,
  changeFloaterVisibility: changeFloaterStatus,
  changeCircleRadius: onChangeCircleRadius,
  changeDarkMode: onChangeDarkMode,
}) {
  const [showSidebar, changeSidebarVisiblity] = useState(false);
  const [isLoggedIn, changeAuthentication] = useState(false);
  const menu = (
    <Menu
      onClick={() => {
        changeAuthentication(false);
      }}
    >
      <Menu.Item key="1">Logout</Menu.Item>
    </Menu>
  );
  return (
    <Layout style={{ height: '100vh' }}>
      <Header className={`header ${darkModeStatus && 'dark-mode'}`}>
        <img className="logo" src={logo} alt="logo" />
        {!isLoggedIn ? (
          <Button type="primary" onClick={() => changeAuthentication(true)}>
            Login
          </Button>
        ) : (
          <Dropdown overlay={menu}>
            <Button type="primary">
              <UserOutlined style={{ fontSize: '20px' }} />
              <DownOutlined />
            </Button>
          </Dropdown>
        )}
      </Header>
      <Content className="site-layout-wrapper">
        <Layout className="site-layout">
          <Sider
            className={`sidebar ${!showSidebar && 'hide-sidebar'} ${
              darkModeStatus && 'dark-mode'
            }`}
          >
            <Menu
              mode="inline"
              defaultSelectedKeys={['1']}
              defaultOpenKeys={['sub1']}
              className={darkModeStatus && 'dark-mode'}
            >
              <CloseCircleOutlined
                className="circle-icon"
                onClick={() => {
                  changeSidebarVisiblity(false);
                }}
              />
              <Menu.Item
                key="1"
                style={{ marginBottom: '100px', marginTop: '10px' }}
              >
                <Switch
                  checked={darkModeStatus}
                  onChange={(val) => onChangeDarkMode(val)}
                  style={{ marginRight: '10px' }}
                />{' '}
                Dark Mode
              </Menu.Item>
              <Menu.Item key="2">
                <Checkbox
                  checked={floatersVisibility}
                  onChange={(e) => changeFloaterStatus(e.target.checked)}
                  style={{ marginRight: '10px' }}
                />
                Show Floaters
              </Menu.Item>
              <SubMenu
                key="sub2"
                icon={<CiCircleOutlined />}
                title="Circle"
                className={darkModeStatus && 'dark-mode'}
              >
                <Menu.Item
                  key="5"
                  className={darkModeStatus && 'dark-mode'}
                  style={{ margin: 0 }}
                >
                  <Checkbox
                    checked={showCircle}
                    onChange={(e) => onChangeCircleStatus(e.target.checked)}
                    style={{ color: darkModeStatus ? 'white' : 'black' }}
                  >
                    Show Circle
                  </Checkbox>
                </Menu.Item>
                <Menu.Item
                  key="6"
                  className={darkModeStatus && 'dark-mode'}
                  style={{ margin: 0 }}
                >
                  <InputNumber
                    min={0}
                    max={1500}
                    style={{ width: '70px', marginRight: '10px' }}
                    value={typeof circleRadius === 'number' ? circleRadius : 0}
                    onChange={(val) => {
                      if (!Number.isNaN(val)) {
                        onChangeCircleRadius(val);
                      }
                    }}
                  />
                  {'  '}
                  Radius (kms)
                </Menu.Item>
              </SubMenu>
            </Menu>
          </Sider>
          <Content className="content-wrapper">
            <AppstoreOutlined
              className={`menu-btn ${darkModeStatus && 'dark-mode'}`}
              onClick={() => {
                changeSidebarVisiblity(true);
              }}
            />
            {floatersVisibility && (
              <div className="forms-wrapper">
                <FormInput />
                <RegionInfo />
              </div>
            )}
            <Map />
          </Content>
        </Layout>
      </Content>
      <Footer className={`footer ${darkModeStatus && 'dark-mode'}`}>
        Cloud Eye ©2021 Created by DEEPAK S M
      </Footer>
    </Layout>
  );
}

App.propTypes = {
  floatersVisibility: PropTypes.bool.isRequired,
  showCircle: PropTypes.bool.isRequired,
  circleRadius: PropTypes.number.isRequired,
  darkModeStatus: PropTypes.bool.isRequired,
  changeCircleStatus: PropTypes.func.isRequired,
  changeCircleRadius: PropTypes.func.isRequired,
  changeFloaterVisibility: PropTypes.func.isRequired,
  changeDarkMode: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    floatersVisibility: state.sidebarActivity.floatersVisibility,
    circleRadius: state.sidebarActivity.circleRadius,
    showCircle: state.sidebarActivity.showCircle,
    darkModeStatus: state.sidebarActivity.darkModeStatus,
  };
};

export default connect(mapStateToProps, {
  changeFloaterVisibility,
  changeCircleRadius,
  changeCircleStatus,
  changeDarkMode,
})(App);
